# 8 VR GUI Best practises

## 1: KISS

Keep It Simple Silly. You always want to keep what you do, as simple as you can. Many reasons exist for this, including the reduction in processing overheads and minimizing codebase size. The simplier your GUI the simplier the user will find it to use

## 2: Use Obvious Default Actions

If the user needs achieve a task, make the action required obvious. Add labels, highlight clickable objects, use obvious elements. Make the user interaction, the default action you would see outside a VR enviroment.

## 3: Ergonomics

Ensure that the use and interaction with the enviroment is comfortable for the user. Though we ideally wish the interaction to be as realistic as possible, it has to be comfortable to perform. If there is no movement, consider having the user seated.

## 4: Follow standard best practises

Within Unity egine and C# there are best practises. Unity has a folder structure within the assets, and C# has its own best practises. Following best practises here helps keep your code and asset structure tidy. 

## 5: Use verbs in Element descriptions

Use clear descriptive doing words. "Open door with your hand" "Push the RED button" "Pull trigger to pick up an item" "Turn around" "Crouch behind the wall". By telling the user what they need to do the game will flow smoother.

## 6: Clearly define what an action will do

Tell or show your user what the action they take will do. If an object can be interacted with, highlight it when they get near it. As we will generally be dealing with objects of types people are accustomed to seeing and interacting with in the real world, most interactions will be obvious for the user. Those that arent, need defining for the user.

## 7: Use simple language

Use language and words that the user will understand. Remember not all users will be programmers, they wont all understand some of the technical names for this. Dont call it a 'Skybox' just call it 'The Sky'. Dont called it a 'Plane' or a 'Terrain' (No thats not a typo, they actually call a plain a plane in Unity) just call it 'The Ground'.

## 8: Follow basic design principals: CRAP

### Contrast

Ensuring that different Elements are coloured in ways to ensure they are visible as seperate elements. Shading needs to suit the situation also.

### Repitition

Use the same colours for the same elements. Use the same interactions for the same element. Repeat elements so they become second nature to the user.

### Alignment

Organized structure in your design will help a user navigate it. In general this refers to a 2D medium, but the same princpals can be used applied to any 2D element in a VR game.

### Proximity 

Refrain from having a user reaching within their space. Have them step to an item or make the item interactable from a larger desitance. Use a movement or Teleport option instead.

## References

[https://unity3d.com/learn/tutorials/topics/best-practices](https://unity3d.com/learn/tutorials/topics/best-practices)

[https://www.ieee.li/pdf/viewgraphs/user_interface_design_best_practices.pdf](https://www.ieee.li/pdf/viewgraphs/user_interface_design_best_practices.pdf)

[https://techcrunch.com/2015/06/21/design-tips-for-vr-games/](https://techcrunch.com/2015/06/21/design-tips-for-vr-games/)

[https://youtu.be/Qwh1LBzz3AU](https://youtu.be/Qwh1LBzz3AU)

[https://2012books.lardbucket.org/books/designing-business-information-systems-apps-websites-and-more/s07-01-c-r-a-p-principles-of-graphic-.html](https://2012books.lardbucket.org/books/designing-business-information-systems-apps-websites-and-more/s07-01-c-r-a-p-principles-of-graphic-.html)

[http://devmag.org.za/2012/07/12/50-tips-for-working-with-unity-best-practices/](http://devmag.org.za/2012/07/12/50-tips-for-working-with-unity-best-practices/)

[https://stackoverflow.com/questions/90813/best-practices-principles-for-gui-design](https://stackoverflow.com/questions/90813/best-practices-principles-for-gui-design)

[https://blog.prototypr.io/designing-for-vr-a-beginners-guide-d2fe37902146](https://blog.prototypr.io/designing-for-vr-a-beginners-guide-d2fe37902146)

